#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <windows.h>
#include "lagrange.h"

#define name_size 20
#define file 0
#define console 1


void freeing_up_memory(double **matrix, int size){
    for (int i = 0; i < size; i++){
        free(matrix[i]);
    }
    free(matrix);
}


void matrix_creation(Matrix* matrix, Resize_matrix* resize_matrix){
    resize_matrix->resize_x = matrix->size_x * resize_matrix->resize;
    resize_matrix->resize_y = matrix->size_y * resize_matrix->resize;

    matrix->matrix = (double **)malloc(matrix->size_x*sizeof(double *));
    for(int i = 0; i < matrix->size_x; i++){
        matrix->matrix[i] = (double *)malloc(matrix->size_y*sizeof(double));
    }

    resize_matrix->resize_matrix = (double **)malloc(resize_matrix->resize_x*sizeof(double *));
    for(int i = 0; i < resize_matrix->resize_x; i++){
        resize_matrix->resize_matrix[i] = (double *)malloc(resize_matrix->resize_y*sizeof(double));
    }
}


void matrix_printing(Matrix* matrix, Resize_matrix* resize_matrix){
    HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
    
    char file_name[name_size];
    printf("\nEnter the name of the destination file: ");
    scanf("%s", &file_name);

    SetConsoleTextAttribute(hOut, 5);
    printf("\nInitial matrix\n");
    for(int i = 0; i < matrix->size_x; i++){
        for (int j = 0; j < matrix->size_y; j++){
            printf("%6.2f", matrix->matrix[i][j]);
        }
        printf("\n");
    }
    printf("\n");

    SetConsoleTextAttribute(hOut, 3);
    FILE *output_file = fopen(file_name, "w");
    printf("Enlarged matrix\n");
    for (int i = 0; i < resize_matrix->resize_x; i++){
            for (int j = 0; j < resize_matrix->resize_y; j++){
                fprintf(output_file,"%7.2f", resize_matrix->resize_matrix[i][j]);
                printf("%7.2f", resize_matrix->resize_matrix[i][j]);
        }
        fprintf(output_file, "\n");
        printf("\n");
    }
    fclose(output_file);
}


int matrix_acquisition(Matrix* matrix, Resize_matrix* resize_matrix){
    int choose;
    printf("\nconsole - 1\nfile - 0\nEnter the matrix setting method: ");
    scanf("%d", &choose);
    char file_name[name_size];
    switch (choose){
        case file:
            printf("\nEnter the name of the matrix file: ");
            scanf("%s", &file_name);
            FILE *values = fopen(file_name, "r");
            if(values == NULL){
                return 1;
            }
            for(int i = 0; i < matrix->size_x; i++){
                for (int j = 0; j < matrix->size_y; j++){
                    fscanf(values, "%lf", &matrix->matrix[i][j]);
                }
            }
            fclose(values);
            break;

        case console:
            printf("\nEnter matrix:\n");
            for(int i = 0; i < matrix->size_x; i++){
                for(int j = 0; j < matrix->size_y; j++){
                    scanf("%lf", &matrix->matrix[i][j]);
                }
            }
            break;

        default:
            printf("You entered something wrong\n");
            matrix_acquisition(matrix, resize_matrix);
            break;
    }
    return 0;
}


int data_request(Matrix* matrix, Resize_matrix* resize_matrix){
    printf("Enter X size of matrix: ");
    scanf("%d", &matrix->size_x);
    printf("\nEnter Y size of matrix: ");
    scanf("%d", &matrix->size_y);
    printf("\nEnter the number how many times increase the size: ");
    scanf("%d", &resize_matrix->resize);
    if (resize_matrix->resize <= 1){
        printf("Increment value must be greater than 1\n");
        return 2;
    }

    matrix_creation(matrix, resize_matrix);
    if (matrix_acquisition(matrix, resize_matrix)){
        printf("Error opening file\n");
        return 1;
    }
    return 0;
}


int main() {
    Matrix matrix;
    Resize_matrix resize_matrix;

    if (data_request(&matrix, &resize_matrix)){
        return 1;
    }
    interpolation(&matrix, &resize_matrix);
    matrix_printing(&matrix, &resize_matrix);
    freeing_up_memory(matrix.matrix, matrix.size_x);
    freeing_up_memory(resize_matrix.resize_matrix, resize_matrix.resize_x);
    return 0;
}