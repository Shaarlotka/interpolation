#ifndef LAGRANGE_H
#define LAGRANGE_H
#include <stdio.h>

typedef struct matrix{
    double **matrix;
    int size_x;
    int size_y;
}Matrix;


typedef struct resize_matrix{
    double **resize_matrix;
    int resize_x;
    int resize_y;
    int resize;
}Resize_matrix;

void interpolation(Matrix* matrix, Resize_matrix* resize_matrix);
void data_transfer(Matrix* matrix, Resize_matrix* resize_matrix);
double* creating_positions(int size, int resize);
void horizontal_interpolation(Matrix* matrix, Resize_matrix* resize_matrix, double *order);
void vertical_interpolation(Matrix* matrix, Resize_matrix* resize_matrix, double *order);
double interpolate_lagrange(double x, double* x_values, double* y_values, int size);
double** create_intermediate_values(Resize_matrix* resize_matrix, int size);

#endif