#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "lagrange.h"


double** create_intermediate_values(Resize_matrix* resize_matrix, int size){
    double **intermediate_values = (double**)malloc(resize_matrix->resize_y*sizeof(double*));
    for(int i = 0; i < resize_matrix->resize_y; i++){
        intermediate_values[i] = (double*)malloc(size*sizeof(double));
    }

    for (int i = 0; i < size; i++){
        for (int j = 0; j < resize_matrix->resize_y; j++){
            intermediate_values[j][i] = resize_matrix->resize_matrix[(i*resize_matrix->resize)+1][j];
        }
    }
    return intermediate_values;
}


double interpolate_lagrange(double x, double* x_values, double* y_values, int size){
	double polynomial = 0;
	for (int i = 0; i < size; i++){
		double factors = 1;
		for (int j = 0; j < size; j++){
			if (j == i){
                continue;
            }
			factors *= (x - x_values[j])/(x_values[i] - x_values[j]);		
		}
		polynomial += factors * y_values[i];
	}
	return polynomial;
}


void vertical_interpolation(Matrix* matrix, Resize_matrix* resize_matrix, double *order){
    double** intermediate_values = create_intermediate_values(resize_matrix, matrix->size_x);

    for (int i = 0; i < resize_matrix->resize_x; i++){
        if (i != i*resize_matrix->resize+1){
            for (int j = 0; j < resize_matrix->resize_y; j++){
                resize_matrix->resize_matrix[i][j] = interpolate_lagrange(i, order, intermediate_values[j], matrix->size_x);
            }
        }
    }
}


void horizontal_interpolation(Matrix* matrix, Resize_matrix* resize_matrix, double *order){
    for (int i = 0; i < matrix->size_x; i++){
        for (int j = 0; j < resize_matrix->resize_y; j++){
            if (j != j*resize_matrix->resize+1){
                resize_matrix->resize_matrix[i*resize_matrix->resize+1][j] = interpolate_lagrange(j, order, matrix->matrix[i], matrix->size_y);
            }
        }
    }
}


double* creating_positions(int size, int resize){
    double *order = (double *)malloc(size*sizeof(double));
    for (int i = 0; i < size; i++){
        order[i] = i*resize+1;
    }
    return order;
}


void data_transfer(Matrix* matrix, Resize_matrix* resize_matrix){
    for (int i = 0; i < matrix->size_x; i++){
        for (int j = 0; j < matrix->size_y; j++){
            resize_matrix->resize_matrix[i*resize_matrix->resize+1][j*resize_matrix->resize+1] = matrix->matrix[i][j];
        }
    }
}


void interpolation(Matrix* matrix, Resize_matrix* resize_matrix){
    double *x_order = creating_positions(matrix->size_y, resize_matrix->resize);
    double *y_order = creating_positions(matrix->size_x, resize_matrix->resize);

    data_transfer(matrix, resize_matrix);
    horizontal_interpolation(matrix, resize_matrix, x_order);
    vertical_interpolation(matrix, resize_matrix, y_order);

    free(x_order);
    free(y_order);
}